#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi
IMAGE_NAME="${IMAGE_NAME:-fedora-toolbox}"
PUSH="${PUSH:-1}"

#DISTRORELEASE=35
FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"

container=$(buildah from registry.fedoraproject.org/fedora-toolbox:$DISTRORELEASE)
mnt=$(buildah mount $container)

dnf --releasever=$DISTRORELEASE --installroot $mnt -y upgrade 
dnf --releasever=$DISTRORELEASE -y install --installroot $mnt \
	fish \
	fzf \
	ripgrep \
	xclip

dnf clean --installroot $mnt all

cp scripts/host-runner.sh $mnt/usr/local/bin/host-runner
buildah run $container -- ln -s /usr/local/bin/host-runner /usr/bin/podman
buildah run $container -- ln -s /usr/local/bin/host-runner /usr/bin/xdg-spawn
buildah run $container -- ln -s /usr/local/bin/host-runner /usr/bin/xdg-user-dir

buildah config --label "io.containers.autoupdate=registry" $container
buildah config --author "Josh Meekhof <jmeekhof@twotheleft.com>" $container
buildah commit --squash $container $FQ_IMAGE_NAME
if [ $PUSH -eq "1" ]; then
	buildah push $FQ_IMAGE_NAME
fi
