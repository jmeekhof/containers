#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi
IMAGE_NAME="${IMAGE_NAME:-ubuntu-toolbox}"
PUSH="${PUSH:-1}"
FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"

container=$(buildah from docker.io/library/ubuntu:20.04)
mnt=$(buildah mount $container)


buildah run $container -- apt-get update
buildah run -e DEBIAN_FRONTEND=noninteractive $container -- apt-get -y install sudo libcap2-bin flatpak-xdg-utils
buildah run $container -- apt-get clean
buildah run $container -- sed -i -e 's/ ALL$/ NOPASSWD:ALL/' /etc/sudoers
buildah run $container -- touch /etc/localtime

cp scripts/ubuntu-host-runner.sh $mnt/usr/local/bin/host-runner
buildah run $container -- ln -s /usr/local/bin/host-runner /usr/bin/vim
buildah run $container -- ln -s /usr/local/bin/host-runner /usr/bin/git
buildah run $container -- ln -s /usr/local/bin/host-runner /usr/bin/nvim
buildah run $container -- ln -s /usr/local/bin/host-runner /usr/bin/podman
buildah run $container -- ln -s /usr/local/bin/host-runner /usr/bin/xdg-spawn

buildah config --author "Josh Meekhof <jmeekhof@twotheleft.com>" $container
buildah config --label "com.github.containers.toolbox='true'" $container
buildah config --cmd "/bin/sh" $container

buildah commit --squash $container $FQ_IMAGE_NAME
if [ $PUSH -eq "1" ]; then
	buildah push $FQ_IMAGE_NAME
fi

