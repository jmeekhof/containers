#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi
IMAGE_NAME="${IMAGE_NAME:-elixir}"
PUSH="${PUSH:-1}"

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"

container=$(buildah from $CI_REGISTRY_IMAGE/fedora-toolbox:latest)
mnt=$(buildah mount $container)

buildah run $container -- dnf --releasever=$DISTRORELEASE -y install \
	https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$DISTRORELEASE.noarch.rpm \
	https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$DISTRORELEASE.noarch.rpm

buildah run $container -- dnf --releasever=$DISTRORELEASE -y install \
	distribution-gpg-keys

buildah run $container -- dnf --releasever=$DISTRORELEASE -y install \
	elixir \
	erlang

buildah run $container -- dnf clean all


buildah commit $container $FQ_IMAGE_NAME
