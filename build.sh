#!/bin/bash
set -o errexit

DISTRORELEASE=35

container=$(buildah from registry.fedoraproject.org/fedora-toolbox:$DISTRORELEASE)
mnt=$(buildah mount $container)

dnf --releasever=$DISTRORELEASE --installroot $mnt upgrade 
dnf --releasever=$DISTRORELEASE -y install --installroot $mnt \
	fish \
	fzf \
	ripgrep \
	xclip

dnf clean --installroot $mnt all

buildah commit $container tools

container=$(buildah from tools)
mnt=$(buildah mount $container)
dnf --releasever=$DISTRORELEASE --installroot $mnt -y install \
	neovim \
	python3-neovim

dnf clean --installroot $mnt all

buildah commit $container neovim 
