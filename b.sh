#!/bin/bash

set -euxo pipefail

export STORAGE_DRIVER=vfs

export BUILDAH_FORMAT=docker

export REGISTRY_AUTH_FILE=${HOME}/auth.json
echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}"

buildah bud -f builds/${IMAGE_NAME} -t ${IMAGE_NAME} .
CONTAINER_ID=$(buildah from $IMAGE_NAME)
buildah commit --squash $CONTAINER_ID $FQ_IMAGE_NAME
buildah push ${FQ_IMAGE_NAME}
