#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi
IMAGE_NAME="${IMAGE_NAME:-beets}"
PUSH="${PUSH:-1}"

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"

container=$(buildah from $CI_REGISTRY_IMAGE/fedora-toolbox:latest)
mnt=$(buildah mount $container)

buildah run $container -- dnf --releasever=$DISTRORELEASE -y install \
	https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$DISTRORELEASE.noarch.rpm \
	https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$DISTRORELEASE.noarch.rpm

buildah run $container -- dnf --releasever=$DISTRORELEASE -y install \
	distribution-gpg-keys

buildah run $container -- dnf --releasever=$DISTRORELEASE -y install \
	python3 \
	python3-pip \
	ffmpeg \
	chromaprint-tools \
	gstreamer1 \
	python3-gstreamer1 \
	python3-gobject \
	gtk3

buildah run $container -- dnf clean all

buildah run $container -- pip3 install beets pyacoustid soco pycairo PyGObject

# The acoustid needs this lib, and it's expected to be in this dir. Now this
# will break when the python version moves from 3.10
if [ -z $mnt/usr/lib64/libpython3.10.so ]; then
	ln -s $mnt/usr/lib64/libpython3.10.so.1.0 $mnt/usr/lib64/libpython3.10.so
fi
buildah run $container -- ln -s /usr/lib64/libpython3.10.so.1.0 /usr/lib64/libpython3.10.so

buildah commit $container $FQ_IMAGE_NAME
