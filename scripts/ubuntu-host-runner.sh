#!/bin/bash
executable=$(basename $0)
set -x
exec /usr/libexec/flatpak-xdg-utils/flatpak-spawn --host $executable "$@"

