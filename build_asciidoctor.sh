#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi
IMAGE_NAME="${IMAGE_NAME:-asciidoctor}"
PUSH="${PUSH:-1}"

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
container=$(buildah from $CI_REGISTRY_IMAGE/fedora-toolbox:latest)
mnt=$(buildah mount $container)
dnf --releasever=$DISTRORELEASE --installroot $mnt -y install \
	rubygem-asciidoctor

dnf clean --installroot $mnt all
buildah commit $container $FQ_IMAGE_NAME

if [ $PUSH -eq "1" ]; then
	buildah push $FQ_IMAGE_NAME
fi
