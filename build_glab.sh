#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi
GOLANG=golang
IMAGE_NAME="${IMAGE_NAME:-glab}"
PUSH="${PUSH:-1}"

FQ_BUILDER_NAME="${CI_REGISTRY_IMAGE}/${GOLANG}:latest"
SRC_URL=https://github.com/profclems/glab
builder=$(buildah from $FQ_BUILDER_NAME)
mnt=$(buildah mount $builder)
mkdir -p $mnt/src
buildah run $builder -- git clone --depth 1 $SRC_URL /src
buildah run --workingdir /src $builder -- make
buildah commit $builder glab_builder


FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
container=$(buildah from $CI_REGISTRY_IMAGE/fedora-toolbox:latest)
mnt=$(buildah mount $container)

buildah copy --from $builder $container /src/bin/glab /usr/bin/glab
buildah commit $container $FQ_IMAGE_NAME

if [ $PUSH -eq "1" ]; then
	buildah push $FQ_IMAGE_NAME
fi
