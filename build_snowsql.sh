#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi

SNOWSQL_URL=https://sfc-repo.snowflakecomputing.com/snowsql/bootstrap/1.2/linux_x86_64/snowflake-snowsql-1.2.24-1.x86_64.rpm
IMAGE_NAME="${IMAGE_NAME:-snowsql}"
PUSH="${PUSH:-1}"
RPM_NAME=$(mktemp --suffix=.rpm -t snowflake_XXX)

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
container=$(buildah from $CI_REGISTRY_IMAGE/fedora-toolbox:latest)
mnt=$(buildah mount $container)
curl -o $RPM_NAME $SNOWSQL_URL
dnf --releasever=$DISTRORELEASE --installroot $mnt -y install $RPM_NAME libxcrypt-compat
rm $RPM_NAME
dnf clean --installroot $mnt
buildah commit $container $FQ_IMAGE_NAME

if [ $PUSH -eq "1" ]; then
	buildah push $FQ_IMAGE_NAME
fi
