#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi
HEROKU_CLI_URL=https://cli-assets.heroku.com/install.sh
IMAGE_NAME="${IMAGE_NAME:-heroku}"
PUSH="${PUSH:-1}"

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
container=$(buildah from $CI_REGISTRY_IMAGE/fedora-toolbox:latest)
mnt=$(buildah mount $container)

mkdir -p $mnt/heroku_install
curl -o $mnt/heroku_install/install.sh $HEROKU_CLI_URL
chmod a+x $mnt/heroku_install/install.sh
buildah run $container -- sh -c /heroku_install/install.sh

buildah commit $container $FQ_IMAGE_NAME

if [ $PUSH -eq "1" ]; then
	buildah push $FQ_IMAGE_NAME
fi
