#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi

GH_REPO=https://cli.github.com/packages/rpm/gh-cli.repo
IMAGE_NAME="${IMAGE_NAME:-gh}"
PUSH="${PUSH:-1}"

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
container=$(buildah from $CI_REGISTRY_IMAGE/fedora-toolbox:latest)
mnt=$(buildah mount $container)

dnf --releasever=$DISTRORELEASE --installroot $mnt config-manager --add-repo $GH_REPO
dnf --releasever=$DISTRORELEASE --installroot $mnt -y install gh git-credential-libsecret

dnf clean --installroot $mnt all
buildah commit $container $FQ_IMAGE_NAME

if [ $PUSH -eq "1" ]; then
	buildah push $FQ_IMAGE_NAME
fi
