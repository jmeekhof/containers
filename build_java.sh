#!/bin/bash
set -euxo pipefail
source common.sh
if [ -f "local.sh" ]; then
	source local.sh
fi

IMAGE_NAME="${IMAGE_NAME:-java}"
PUSH="${PUSH:-1}"

GRADLE_VERSION="${GRADLE_VERSION:-7.3.3}"

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
container=$(buildah from $CI_REGISTRY_IMAGE/fedora-toolbox:${CI_COMMIT_REF_SLUG})
mnt=$(buildah mount $container)

dnf --releasever=$DISTRORELEASE --installroot $mnt -y install java-11-openjdk
dnf clean --installroot $mnt all

TMPFILE=$(mktemp --suffix=.zip)
curl "https://downloads.gradle-dn.com/distributions/gradle-${GRADLE_VERSION}-bin.zip" -o $TMPFILE

unzip -q -d $mnt/usr/local $TMPFILE
mv $mnt/usr/local/gradle-${GRADLE_VERSION} $mnt/usr/local/gradle


IMAGEPATH=$(buildah run $container printenv PATH)
buildah config --env PATH=$IMAGEPATH:/usr/local/gradle/bin $container

buildah commit $container $FQ_IMAGE_NAME

if [ $PUSH -eq "1" ]; then
	buildah push $FQ_IMAGE_NAME
fi
